FROM bitwalker/alpine-elixir:1.5.2

RUN \
  apk add --no-cache gcc musl-dev build-base file ca-certificates openssh
